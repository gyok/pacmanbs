#ifndef LABYRINTHVIEW_H
#define LABYRINTHVIEW_H

#include <QOpenGLWidget>
#include <QPainter>
#include <cmath>
#include <iostream>
#include "../controller/converter.h"
#include "../model/labyrinth.h"

class LabyrinthView : public QOpenGLWidget
{
    Q_OBJECT
public:
    explicit LabyrinthView(Labyrinth*, QWidget *parent = nullptr);
protected:
    virtual void initializeGL();
    virtual void resizeGL(int w, int h);
    virtual void paintGL();
private:
    Labyrinth* _labyrinthData;

    void drawLabyrinth();
};

#endif // LABYRINTHVIEW_H
