#ifndef VIEW_H
#define VIEW_H

#include <QMainWindow>
#include <QHBoxLayout>
#include <QApplication>
#include <QStyle>
#include <QDesktopWidget>
#include "../controller/controller.h"
#include "labyrinthView.h"

#define LABYRINTHSIZE 500
#define APPWIDTH 500
#define APPHEIGHT 500

class View : public QMainWindow
{
    Q_OBJECT
public:
    explicit View(Controller*, QWidget *parent = nullptr);

signals:

public slots:
private:
    int centerAndResizeWindow();

    Controller* _controller;
    LabyrinthView* _labyrinth;
};

#endif // VIEW_H
