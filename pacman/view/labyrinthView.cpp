#include "labyrinthView.h"

LabyrinthView::LabyrinthView(Labyrinth* labyrinth, QWidget* pwgt /*= 0*/) : QOpenGLWidget(pwgt)
{
    _labyrinthData = labyrinth;
    glEnable(GL_MULTISAMPLE);
}


void LabyrinthView::initializeGL()
{
}

void LabyrinthView::resizeGL(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, w, h);
    glOrtho(0, w, h, 0, -1, 1);
}

void LabyrinthView::paintGL()
{
    QPainter* painter = new QPainter(this);
    painter->beginNativePainting();
    glClearColor(0.97f, 0.98f, 0.98f, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    drawLabyrinth();

    painter->endNativePainting();
    painter->~QPainter();
}


void LabyrinthView::drawLabyrinth() {
    int lWidth = _labyrinthData->GetWidth();
    int lHeight = _labyrinthData->GetHeight();

    // cast to double to note lose data in division
    int cellHeight = static_cast<int>(ceil(static_cast<double>(this->height()) / lHeight));
    int cellWidth = static_cast<int>(ceil(static_cast<double>(this->width()) / lWidth));

    Cell** labyrinth = _labyrinthData->GetLabyrinth();

    for (int i = 0; i < lHeight; i++) {
        for (int j = 0; j < lWidth; j++) {
            int cellPosX = static_cast<int>(ceil(j * cellWidth));
            int cellPosY = static_cast<int>(ceil(i * cellHeight));
            Cell::Pos pos = Cell::Pos(cellPosX, cellPosY);
            Converter::convert(&labyrinth[i][j])
                    ->Draw(cellHeight, cellWidth, pos);
        }
    }
}
