#ifndef CELLVIEW_H
#define CELLVIEW_H

#include "../../model/cell.h"
#include <QColor>
#include <QOpenGLWidget>

class CellView
{
    GLfloat *_colorR = new GLfloat(0.45f);
    GLfloat *_colorG = new GLfloat(0.45f);
    GLfloat *_colorB = new GLfloat(0.45f);
public:
    virtual void Draw(int height, int width, Cell::Pos&);

    CellView(GLfloat*, GLfloat*, GLfloat*);
    virtual ~CellView();
};

#endif // CELLVIEW_H
