#ifndef ENERGISERCELLVIEW_H
#define ENERGISERCELLVIEW_H

#include "foodcellview.h"

#define ENERGISERSIZE 0.4

class EnergiserCellView : public FoodCellView
{
public:
    EnergiserCellView();
};

#endif // ENERGISERCELLVIEW_H
