#include "cellview.h"


void CellView::Draw(int height, int width, Cell::Pos& pos) {
    glBegin(GL_POLYGON);

    glColor3f(*_colorR, *_colorG, *_colorB);

    glVertex2f(pos.x, pos.y);
    glVertex2f(pos.x + width, pos.y);
    glVertex2f(pos.x + width, pos.y + height);
    glVertex2f(pos.x, pos.y + height);

    glEnd();
}

CellView::CellView(GLfloat *colorR, GLfloat *colorG, GLfloat *colorB) {
    _colorR = colorR;
    _colorG = colorG;
    _colorB = colorB;
}


CellView::~CellView() {}
