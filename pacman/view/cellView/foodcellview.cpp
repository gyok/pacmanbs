#include "foodcellview.h"


void FoodCellView::Draw(int height, int width, Cell::Pos& pos) {
    // draw black road
    CellView::Draw(height, width, pos);

    // draw food
    glBegin(GL_POLYGON);

    int a = width /2;
    int b = height /2;
    Cell::Pos centerPos = Cell::Pos(width / 2 + pos.x,
                                    height / 2 + pos.y);
    glColor3f(*_foodColorR, *_foodColorG, *_foodColorB);
    int angleStep = FOODANGLESTEP;
    for (int angle = 0; angle < FOOLCIRCLE; angle += angleStep) {
        float a1 = centerPos.x + width  / 2.0 * cos(angle * PI / 180)  * _foodSize;
        float b1 = centerPos.y + height / 2.0 * sin(angle * PI / 180) * _foodSize;
        glVertex2f(static_cast<GLfloat>(centerPos.x + width  / 2.0 * cos(angle * PI / 180) * _foodSize),
                   static_cast<GLfloat>(centerPos.y + height / 2.0 * sin(angle * PI / 180) * _foodSize));
    }

    glEnd();
}

FoodCellView::FoodCellView()
    : RoadCellView()
{
    _foodColorR = new GLfloat(0.97f);
    _foodColorG = new GLfloat(0.83f);
    _foodColorB = new GLfloat(1);

    _foodSize = FOODSIZE;
}
