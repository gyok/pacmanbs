#ifndef FOODCELLVIEW_H
#define FOODCELLVIEW_H

#include <cmath>
#include "roadcellview.h"

#define FOOLCIRCLE 360
#define FOODANGLESTEP FOOLCIRCLE / 15;
#define FOODSIZE 0.2
#define PI 3.14159265

class FoodCellView : public RoadCellView
{
protected:
    GLfloat* _foodColorR;
    GLfloat* _foodColorG;
    GLfloat* _foodColorB;
    double _foodSize;
public:
    virtual void Draw(int height, int width, Cell::Pos&);
    FoodCellView();
};

#endif // FOODCELLVIEW_H
