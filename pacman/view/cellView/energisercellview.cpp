#include "energisercellview.h"

EnergiserCellView::EnergiserCellView()
    : FoodCellView ()
{
    _foodColorR = new GLfloat(0.25f);
    _foodColorG = new GLfloat(0.90f);
    _foodColorB = new GLfloat(0.95f);

    _foodSize = ENERGISERSIZE;
}
