#include "view.h"

View::View(Controller* controller, QWidget *parent) : QMainWindow(parent)
{
    _controller = controller;
    QWidget* mainWindow = new QWidget;

    _labyrinth = new LabyrinthView(_controller->GetLabyrinth(), this);
    _labyrinth->resize(LABYRINTHSIZE, LABYRINTHSIZE);

    QHBoxLayout *mainLayout = new QHBoxLayout();
    mainLayout->addWidget(_labyrinth);

    mainWindow->setLayout(mainLayout);
    this->setCentralWidget(mainWindow);
    centerAndResizeWindow();
}


int View::centerAndResizeWindow()
{
    QRect desktopSize = QApplication::desktop()->screenGeometry();
    QSize appSize = QSize(APPWIDTH, APPHEIGHT);
    this->setGeometry(QStyle::alignedRect(Qt::LeftToRight,
                                          Qt::AlignCenter,
                                          appSize,
                                          desktopSize));

    return 0;
}
