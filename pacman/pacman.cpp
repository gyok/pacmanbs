#include "pacman.h"

Pacman::Pacman() {
    Model *model = new Model();
    Controller *controller = new Controller(model);
    View *view = new View(controller);

    view->show();
}

Pacman::~Pacman()
{

}
