#ifndef MODEL_H
#define MODEL_H

#include "labyrinth.h"

class Model
{
public:
    Labyrinth* GetLabyrinth();
    Model();
private:
    Labyrinth* _labyrinth;
};

#endif // MODEL_H
