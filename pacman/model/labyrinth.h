#ifndef LABYRINTH_H
#define LABYRINTH_H

#include <iostream>
#include "cell.h"

class Labyrinth
{
public:
    int GetHeight();
    int GetWidth();
    Cell** GetLabyrinth();

    Labyrinth();
private:
    static Cell** readLabyrinth();
    static Cell* getDoor(Cell**);
    static Cell::CellType getCellTypeByChar(char);

    Cell *_door;
    Cell** _labyrinth;

    static const int HEIGHT = 22;
    static const int WIDTH = 19;
    int _height;
    int _width;
};

#endif // LABYRINTH_H
