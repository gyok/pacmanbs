#include "cell.h"

Cell::Pos::Pos(int x, int y) {
    this->x = x;
    this->y = y;
}

Cell::Cell()
{
}


Cell::CellType Cell::GetType() {
    return _type;
}


void Cell::SetType(Cell::CellType type) {
    _type = type;
}


Cell::Pos Cell::GetPos() {
    return *_pos;
}


void Cell::SetPos(Cell::Pos pos) {
    _pos->x = pos.x;
    _pos->y = pos.y;
}
