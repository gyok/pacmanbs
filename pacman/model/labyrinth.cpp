#include "labyrinth.h"


int Labyrinth::GetHeight() {
    return HEIGHT;
}


int Labyrinth::GetWidth() {
    return  WIDTH;
}


Cell** Labyrinth::GetLabyrinth() {
    return _labyrinth;
}


Labyrinth::Labyrinth()
{
    _labyrinth = Labyrinth::readLabyrinth();
    _door = Labyrinth::getDoor(_labyrinth);
}


Cell** Labyrinth::readLabyrinth() {
    Cell** labyrinth = new Cell* [HEIGHT];
    char labyrinthPrototype[HEIGHT][WIDTH] = {
        {'w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w'},
        {'w','f','f','f','f','f','f','f','f','w','f','f','f','f','f','f','f','f','w'},
        {'w','e','w','w','f','w','w','w','f','w','f','w','w','w','f','w','w','e','w'},
        {'w','f','w','w','f','w','w','w','f','w','f','w','w','w','f','w','w','f','w'},
        {'w','f','f','f','f','f','f','f','f','f','f','f','f','f','f','f','f','f','w'},
        {'w','f','w','w','f','w','f','w','w','w','w','w','f','w','f','w','w','f','w'},
        {'w','f','f','f','f','w','f','f','f','w','f','f','f','w','f','f','f','f','w'},
        {'w','w','w','w','f','w','w','w','r','w','r','w','w','w','f','w','w','w','w'},
        {'w','w','w','w','f','w','r','r','r','r','r','r','r','w','f','w','w','w','w'},
        {'w','w','w','w','f','w','r','w','w','d','w','w','r','w','f','w','w','w','w'},
        {'r','r','r','r','f','r','r','w','r','r','r','w','r','r','f','r','r','r','r'},
        {'w','w','w','w','f','w','r','w','w','w','w','w','r','w','f','w','w','w','w'},
        {'w','w','w','w','f','w','r','r','r','r','r','r','r','w','f','w','w','w','w'},
        {'w','w','w','w','f','w','r','w','w','w','w','w','r','w','f','w','w','w','w'},
        {'w','f','f','f','f','f','f','f','f','w','f','f','f','f','f','f','f','f','w'},
        {'w','f','w','w','f','w','w','w','f','w','f','w','w','w','f','w','w','f','w'},
        {'w','e','f','w','f','f','f','f','f','f','f','f','f','f','f','w','f','e','w'},
        {'w','w','f','w','f','w','f','w','w','w','w','w','f','w','f','w','f','w','w'},
        {'w','f','f','f','f','w','f','f','f','w','f','f','f','w','f','f','f','f','w'},
        {'w','f','w','w','w','w','w','w','f','w','f','w','w','w','w','w','w','f','w'},
        {'w','f','f','f','f','f','f','f','f','f','f','f','f','f','f','f','f','f','w'},
        {'w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w','w'}
    };

    Cell::CellType type = Cell::CellType::Wall;
    for (int i = 0; i < HEIGHT; i++) {
        labyrinth[i] = new Cell[WIDTH];
        for (int j = 0; j < WIDTH; j++) {
            type = getCellTypeByChar(labyrinthPrototype[i][j]);
            labyrinth[i][j].SetType(type);
        }
    }

    return labyrinth;
}


Cell* Labyrinth::getDoor(Cell** labyrinth) {
    Cell *door = &labyrinth[0][0];
    for (int i = 0; i < HEIGHT; i++) {
        for (int j = 0; j < WIDTH; j++) {
            if (labyrinth[i][j].GetType() == Cell::CellType::Door) {
                door = &labyrinth[i][j];
                break;
            }
        }
    }

    return door;
}


Cell::CellType Labyrinth::getCellTypeByChar(char letter) {
    Cell::CellType type = Cell::CellType::Wall;
    switch (letter) {
    case 'w':
        type = Cell::CellType::Wall;
        break;
    case 'f':
        type = Cell::CellType::Food;
        break;
    case 'e':
        type = Cell::CellType::Energiser;
        break;
    case 'd':
        type = Cell::CellType::Door;
        break;
    default:
        type = Cell::CellType::Road;
    }

    return type;
}
