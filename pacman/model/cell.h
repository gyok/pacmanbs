#ifndef CELL_H
#define CELL_H


class Cell
{

public:
    enum CellType {
        Wall,
        Food,
        Energiser,
        Door,
        Road
    };
    struct Pos {
        int x;
        int y;

        Pos(int, int);
    };

    CellType GetType();
    void SetType(CellType);

    Pos GetPos();
    void SetPos(Pos);

    Cell();
private:
    CellType _type;
    Pos *_pos = new Pos(0, 0);

    const int _size = 30;
};

#endif // CELL_H
