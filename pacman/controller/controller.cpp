#include "controller.h"

Controller::Controller(Model* model)
{
    _model = model;
}


Labyrinth* Controller::GetLabyrinth() {
    return _model->GetLabyrinth();
}
