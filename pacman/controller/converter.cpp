#include "converter.h"


CellView* Converter::convert(Cell* cell) {
    switch (cell->GetType()) {
    case Cell::CellType::Wall:
        return new WallCellView();
    case Cell::CellType::Food:
        return new FoodCellView();
    case Cell::CellType::Energiser:
        return new EnergiserCellView();
    case Cell::CellType::Door:
        return new DoorCellView();
    case Cell::CellType::Road:
        return new RoadCellView();
    }
}
