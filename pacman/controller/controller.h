#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "../model/model.h"

class Controller
{
public:
    Controller(Model* model);
    Labyrinth* GetLabyrinth();
private:
    Model* _model;
};

#endif // CONTROLLER_H
