#ifndef CONVERTER_H
#define CONVERTER_H

#include "../model/cell.h"
#include "../view/cellView/wallcellview.h"
#include "../view/cellView/foodcellview.h"
#include "../view/cellView/energisercellview.h"
#include "../view/cellView/doorcellview.h"
#include "../view/cellView/roadcellview.h"


class Converter
{
public:
    static CellView* convert(Cell*);

    Converter() = delete;
};

#endif // CONVERTER_H
